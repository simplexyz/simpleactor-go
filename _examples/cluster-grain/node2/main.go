package main

import (
	"fmt"

	"cluster-grain/shared"

	"gitee.com/simplexyz/simpleactor-go/actor"
	"gitee.com/simplexyz/simpleactor-go/cluster"
	"gitee.com/simplexyz/simpleactor-go/cluster/clusterproviders/consul"
	"gitee.com/simplexyz/simpleactor-go/cluster/identitylookup/disthash"
	"gitee.com/simplexyz/simpleactor-go/remote"
	console "github.com/asynkron/goconsole"
)

type HelloGrain struct{}

func (h HelloGrain) Init(ctx cluster.GrainContext)           {}
func (h HelloGrain) Terminate(ctx cluster.GrainContext)      {}
func (h HelloGrain) ReceiveDefault(ctx cluster.GrainContext) {}

func (h HelloGrain) SayHello(request *shared.HelloRequest, ctx cluster.GrainContext) (*shared.HelloResponse, error) {
	return &shared.HelloResponse{Message: "Hello " + request.Name}, nil
}

func main() {
	system := actor.NewActorSystem()
	provider, _ := consul.New()
	lookup := disthash.New()
	remoteConfig := remote.Configure("localhost", 0)
	helloKind := shared.NewHelloKind(func() shared.Hello {
		return &HelloGrain{}
	}, 0)
	clusterConfig := cluster.Configure("my-cluster", provider, lookup, remoteConfig,
		cluster.WithKinds(helloKind))

	c := cluster.New(system, clusterConfig)
	c.StartMember()
	fmt.Print("\nBoot other nodes and press Enter\n")
	console.ReadLine()
	c.Shutdown(true)
}
