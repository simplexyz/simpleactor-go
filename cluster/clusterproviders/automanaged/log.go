package automanaged

import "gitee.com/simplexyz/simpleactor-go/log"

var plog = log.New(log.DebugLevel, "[AUTOMANAGED]")

// SetLogLevel sets the log level for the logger
// SetLogLevel is safe to be called concurrently
func SetLogLevel(level log.Level) {
	plog.SetLevel(level)
}
