module opentelemetry-custom-provider

go 1.21

toolchain go1.21.3

replace gitee.com/simplexyz/simpleactor-go => ../..

require (
	gitee.com/simplexyz/simpleactor-go v0.0.0-00010101000000-000000000000
	github.com/asynkron/goconsole v0.0.0-20160504192649-bfa12eebf716
	go.opentelemetry.io/otel/exporters/stdout/stdoutmetric v0.27.0
	go.opentelemetry.io/otel/metric v1.16.0
	go.opentelemetry.io/otel/sdk/metric v0.39.0
)
