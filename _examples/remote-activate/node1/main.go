package main

import (
	"fmt"
	"time"

	"remoteactivate/messages"

	"gitee.com/simplexyz/simpleactor-go/actor"
	"gitee.com/simplexyz/simpleactor-go/remote"
	console "github.com/asynkron/goconsole"
)

func main() {
	timeout := 5 * time.Second

	system := actor.NewActorSystem()
	remoteConfig := remote.Configure("127.0.0.1", 8081)
	r := remote.NewRemote(system, remoteConfig)
	r.Start()

	rootContext := system.Root

	// props := actor.
	// 	PropsFromFunc(func(context actor.Context) {
	// 		switch context.Message().(type) {
	// 		case *actor.Started:
	// 			log.Printf("actor started " + context.Self().String())
	// 		case *messages.HelloRequest:
	// 			log.Println("Received pong from sender")
	// 			message := &messages.HelloResponse{Message: "hello from remote"}
	// 			context.Request(context.Sender(), message)
	// 		}
	// 	})

	pidResp, _ := r.SpawnNamed("127.0.0.1:8080", "world", "hello", timeout)

	res, _ := rootContext.RequestFuture(pidResp.Pid, &messages.HelloRequest{}, timeout).Result()

	response := res.(*messages.HelloResponse)

	fmt.Printf("Response from remote %v, sender=%s", response.Message, pidResp.Pid.String())

	console.ReadLine()
}
