package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"cluster-restartgracefully/shared"

	"gitee.com/simplexyz/simpleactor-go/cluster/identitylookup/disthash"

	"gitee.com/simplexyz/simpleactor-go/actor"
	"gitee.com/simplexyz/simpleactor-go/cluster"
	"gitee.com/simplexyz/simpleactor-go/cluster/clusterproviders/consul"
	"gitee.com/simplexyz/simpleactor-go/log"
	"gitee.com/simplexyz/simpleactor-go/remote"
)

var (
	plog     = log.New(log.DebugLevel, "[Example]")
	system   = actor.NewActorSystem()
	_cluster *cluster.Cluster
)

func main() {
	provider := flag.String("provider", "consul", "clients count.")
	actorTTL := flag.Duration("ttl", 10*time.Second, "time to live of actor.")
	port := flag.Int("port", 0, "listen port.")

	flag.Parse()
	startNode(*port, *provider, *actorTTL)

	// waiting CTRL-C
	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, syscall.SIGINT)
	for sig := range sigCh {
		switch sig {
		case syscall.SIGINT:
			plog.Info("Shutdown...")
			_cluster.Shutdown(true)
			plog.Info("Shutdown ok")
			time.Sleep(time.Second)
			os.Exit(0)
		default:
			plog.Info("Skipping", log.Object("sig", sig))
		}
	}
}

func startNode(port int, provider string, timeout time.Duration) {
	plog.Info("press 'CTRL-C' to shutdown server.")
	shared.CalculatorFactory(func() shared.Calculator {
		return &CalcGrain{}
	})

	var cp cluster.ClusterProvider
	var err error
	switch provider {
	case "consul":
		cp, err = consul.New()
	// case "etcd":
	//	cp, err = etcd.New()
	default:
		panic(fmt.Errorf("invalid provider:%s", provider))
	}

	id := disthash.New()

	if err != nil {
		panic(err)
	}

	remoteCfg := remote.Configure("127.0.0.1", port)
	cfg := cluster.Configure("cluster-restartgracefully", cp, id, remoteCfg, cluster.WithKinds(shared.GetCalculatorKind()))
	_cluster = cluster.New(system, cfg)
	_cluster.StartMember()
}
