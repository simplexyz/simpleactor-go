package main

import (
	"fmt"

	"cluster-broadcast/shared"

	"gitee.com/simplexyz/simpleactor-go/actor"
	"gitee.com/simplexyz/simpleactor-go/cluster"
	"gitee.com/simplexyz/simpleactor-go/cluster/clusterproviders/consul"
	"gitee.com/simplexyz/simpleactor-go/cluster/identitylookup/disthash"
	"gitee.com/simplexyz/simpleactor-go/remote"
	console "github.com/asynkron/goconsole"
)

func main() {
	cluster := startNode()

	fmt.Print("\nBoot other nodes and press Enter\n")
	console.ReadLine()

	cluster.Shutdown(true)
}

func startNode() *cluster.Cluster {
	system := actor.NewActorSystem()

	provider, _ := consul.New()
	lookup := disthash.New()
	config := remote.Configure("localhost", 0)

	props := actor.PropsFromFunc(func(ctx actor.Context) {
		switch msg := ctx.Message().(type) {
		case *actor.Started:
			fmt.Printf("Started %v", msg)
		case *shared.Hello:
			fmt.Printf("Hello %v\n", msg.Name)
		}
	})
	helloKind := cluster.NewKind("hello", props)
	clusterConfig := cluster.Configure("my-cluster", provider, lookup, config, cluster.WithKinds(helloKind))
	c := cluster.New(system, clusterConfig)

	c.StartMember()
	return c
}
