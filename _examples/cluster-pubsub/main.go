package main

import (
	"strconv"

	"gitee.com/simplexyz/simpleactor-go/actor"
	"gitee.com/simplexyz/simpleactor-go/cluster"
	"gitee.com/simplexyz/simpleactor-go/cluster/clusterproviders/test"
	"gitee.com/simplexyz/simpleactor-go/cluster/identitylookup/disthash"
	"gitee.com/simplexyz/simpleactor-go/remote"
	console "github.com/asynkron/goconsole"
)

func main() {
	c := startNode()

	for i := 0; i < 3; i++ {
		GetUserActorGrainClient(c, "user"+strconv.Itoa(i)).Connect(&Empty{})
	}

	console.ReadLine()
	c.Shutdown(true)
}

func startNode() *cluster.Cluster {
	// how long before the grain poisons itself
	system := actor.NewActorSystem()

	provider := test.NewTestProvider(test.NewInMemAgent())
	lookup := disthash.New()
	config := remote.Configure("localhost", 0)

	userKind := NewUserActorKind(func() UserActor {
		return &User{}
	}, 0)

	clusterConfig := cluster.Configure("my-cluster", provider, lookup, config,
		cluster.WithKinds(userKind))

	cluster := cluster.New(system, clusterConfig)

	cluster.StartMember()

	return cluster
}
