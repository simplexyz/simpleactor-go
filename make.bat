@setlocal enabledelayedexpansion
@setlocal enableextensions

::@set PACKAGE_PATH=gitee.com\simplexyz\simpleactor-go
@set WORK_DIR=%~dp0
::@set GOPATH=%WORK_DIR%
::@set GOPROXY=https://goproxy.cn

@IF "%1" == "" call :all & cd %WORK_DIR% & goto :exit

@IF "%1" == "all" call :all & cd %WORK_DIR% & goto :exit

@IF "%1" == "clean-log" call :clean-log & cd %WORK_DIR% & goto :exit

@IF "%1" == "mod-tidy" call :mod-tidy & cd %WORK_DIR% & goto :EXIT

@IF "%1" == "update-proto" call :update-proto & cd %WORK_DIR% & goto :exit

@echo unsupported operate [%1]

@goto :exit


:all
@echo make all begin
@echo make all end
@goto :exit


:clean-log
del /f /s /q /a *.log
@goto exit


:mod-tidy
@echo [mod-tidy] begin
@echo off
@for /R %WORK_DIR% %%f in (*.mod) do (
    @set "GO_MOD_FILE_DIR=%%~dpf"
    @cd !GO_MOD_FILE_DIR!
    echo go mod tidy in [!GO_MOD_FILE_DIR!]
    go mod tidy
)
@echo on
@echo [mod-tidy] end
@goto :EXIT

:update-proto
@echo [update-proto] begin
go get -u github.com/gogo/protobuf/proto
go get -u github.com/gogo/protobuf/protoc-gen-gogo
go get -u github.com/gogo/protobuf/gogoproto
go get -u github.com/gogo/protobuf/protoc-gen-gofast
go get -u google.golang.org/grpc
go get -u github.com/gogo/protobuf/protoc-gen-gogofast
go get -u github.com/gogo/protobuf/protoc-gen-gogofaster
go get -u github.com/gogo/protobuf/protoc-gen-gogoslick
@echo [update-proto] end
@goto :exit

:exit
