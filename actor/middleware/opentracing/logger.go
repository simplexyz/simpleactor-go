package opentracing

import "gitee.com/simplexyz/simpleactor-go/log"

var logger = log.New(log.ErrorLevel, "[TRACING]")
