package actor

import "sync"

const PH = 1

func (m *PID) ResetEx() {
	if m == nil {
		return
	}
	m.Address = ""
	m.ID = ""
	m.p = nil
}

func (m *PID) Clone() *PID {
	if m == nil {
		return nil
	}
	return &PID{
		Address: m.Address,
		ID:      m.ID,
		p:       m.p,
	}
}

func Clone_PID_Slice(dest []*PID, src []*PID) []*PID {
	if len(src) > 0 {
		dest = make([]*PID, len(src))
		for i, e := range src {
			if e != nil {
				dest[i] = e.Clone()
			}
		}
	} else {
		//dest = []*PID{}
		dest = nil
	}
	return dest
}

var g_PID_Pool = sync.Pool{}

func Get_PID() *PID {
	m, ok := g_PID_Pool.Get().(*PID)
	if !ok {
		m = NewPID("", "")
	} else {
		if m == nil {
			m = NewPID("", "")
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_PID(i interface{}) {
	if m, ok := i.(*PID); ok && m != nil {
		g_PID_Pool.Put(i)
	}
}
