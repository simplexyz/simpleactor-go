package main

import (
	"runtime"

	"gitee.com/simplexyz/simpleactor-go/actor"
	"gitee.com/simplexyz/simpleactor-go/remote"
	console "github.com/asynkron/goconsole"
)

var (
	system  = actor.NewActorSystem()
	context = system.Root
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	props := actor.PropsFromFunc(func(ctx actor.Context) {})
	cfg := remote.Configure("127.0.0.1", 8080, remote.WithKinds(remote.NewKind("remote", props)))

	r := remote.NewRemote(system, cfg)
	r.Register("remote", props)
	r.Start()

	// empty actor just to have something to remote spawn

	console.ReadLine()
}
